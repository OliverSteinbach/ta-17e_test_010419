const users = require("./user");	
	
test("Exception", () => {

	expect(() => {

		users("string");

	}).toThrowError("id needs to be integer");

});
